# README #

### What is this repository for? ###

* It's useful to be able to predict what an investment is likely to be worth in the future.
* This module performs calculations to allow determination of future value of properties. 
* Version 0.1.0
* [Source for Simple Future Value Formula](http://www.investopedia.com/walkthrough/corporate-finance/3/time-value-money/future-value.aspx)

### Simple Future Value --> (Pn) = (P0)((1+r)^nt) ###
* (Pn) is future value of P0
* (P0) is original amount invested
* r is the rate of interest
* n is the number of compounding periods per year
* t is the term in years

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact