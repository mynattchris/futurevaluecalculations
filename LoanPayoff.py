#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Loan Payoff Script
#  LoanPayoff.py
	
def amortizationTable(startingPrincipal, yearlyInterestRate, termMonths, standardPmt):
	global currentPrincipal
	for x in range(0, termMonths):
		monthlyInterestRate = yearlyInterestRate/12
		monthlyInterestPmt = monthlyInterestRate*currentPrincipal
		principlePayment = standardPmt-monthlyInterestPmt
		currentPrincipal = startingPrincipal-principlePayment
		if currentPrincipal <= 0:
			break
	return currentPrincipal

def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))

