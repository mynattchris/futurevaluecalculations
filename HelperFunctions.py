#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys


def futureValueCalc(szPresValue, szInterestRate, szNumCompounds, szMonths):
	futureValue = szPresValue*((1+szInterestRate)**szNumCompoundMonths)
	return futureValue
 

def standardPayment(principle, yearlyInterestRate, termMonths):
	global standardPmt
	monthlyInterestRate = yearlyInterestRate/12
	standardPmt = (principle*monthlyInterestRate)/(1-((1+monthlyInterestRate)**termMonths))
	return standardPmt
 
 
def main(argv=None):
	if argv is None:
		argv = sys.argv
		
if __name__ == "__main__":
	sys.exit(main())
